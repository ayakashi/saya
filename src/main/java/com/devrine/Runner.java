package com.devrine;

import com.devrine.util.Worker;

public class Runner {
    /**
     * This method is used to start bot locally
     * @param args default parameter, not used
     */
    public static void main(String[] args) {
        Worker.start();
    }
}
