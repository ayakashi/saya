package com.devrine.controllers;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.devrine.util.Worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class MainController implements ServletContextListener {
    private static final Logger logger = LoggerFactory.getLogger(MainController.class);

    public void contextInitialized(ServletContextEvent event) {
        Worker.start();
        logger.info("Started");
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        Worker.stop();
        logger.info("Stopped");
    }
}
