package com.devrine.listeners;

import com.devrine.constants.enumerations.command.Command;
import com.devrine.listeners.handlers.impl.*;
import com.devrine.listeners.handlers.impl.greetings.GreetingsResetHandler;
import com.devrine.listeners.handlers.impl.greetings.GreetingsSetHandler;
import com.devrine.listeners.handlers.impl.util.CreateRankRolesHandler;
import com.devrine.listeners.handlers.impl.util.UpdateIdsHandler;
import com.devrine.listeners.handlers.impl.util.UpdatePuuidsHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.EmbedFormatter;
import com.devrine.util.permissions.PermissionsUtil;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.MissingPermissionsException;
import sx.blah.discord.util.RateLimitException;

public class EventListener {
    private static IDiscordClient client;

    public static void setClient(IDiscordClient client) {
        EventListener.client = client;
    }

    @EventSubscriber
    public void onReadyEvent(ReadyEvent event) {
        Discord4J.LOGGER.info("Bot is ready.");
    }

    @EventSubscriber
    public void onUserJoinEvent(UserJoinEvent event) {
        IUser user = event.getUser();
        IGuild guild = event.getGuild();
        IChannel channel = guild.getDefaultChannel();

        DBManager.executeConfig(guild.getDefaultChannel(), configDao -> {
            String greetings = configDao.getGreetings(guild.getStringID());
            if (greetings == null) {
                EmbedFormatter.sendDefaultGreetings(channel, user);
            } else if (!greetings.isEmpty()) {
                EmbedFormatter.sendGreetings(channel, greetings, user);
            }
        });
    }

    @EventSubscriber
    public void onMessageReceivedEvent(MessageReceivedEvent event) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();

        String content = message.getContent();
        if (channel.isPrivate() || content == null || !content.startsWith("--")) {
            return;
        }
        Command cmd;
        int spacePos;
        try {
            spacePos = content.indexOf(' ');
            if (spacePos > 0) {
                content = content.substring(0, spacePos);
            }

            cmd = Command.getByName(content);
            if(cmd != null && cmd.isImplemented() && PermissionsUtil.isAllowed(cmd, event.getAuthor().getPermissionsForGuild(event.getGuild()))) {
                switch (cmd) {
                    case HELP:
                        new HelpHandler().handle(event, client);
                        break;
                    case PING:
                        new PingHandler().handle(event, client);
                        break;
                    case USER_INFO:
                        new UserInfoHandler().handle(event, client);
                        break;
                    case RANK:
                        new RankHandler().handle(event, client);
                        break;
                    case UNKNOWN:
                        new UnknownHandler().handle(event, client);
                        break;
                    case SET_LANGUAGE:
                        new LanguageSetHandler().handle(event, client);
                        break;
                    case SET_GREETINGS:
                        new GreetingsSetHandler().handle(event, client);
                        break;
                    case RESET_GREETINGS:
                        new GreetingsResetHandler().handle(event, client);
                        break;
                    case UPDATE_IDS:
                        new UpdateIdsHandler().handle(event, client);
                        break;
                    case UPDATE_PUUIDS:
                        new UpdatePuuidsHandler().handle(event, client);
                        break;
                    case ADD_USER:
                        new UserAddHandler().handle(event, client);
                        break;
                    case LIST:
                        new UserListHandler().handle(event, client);
                        break;
                    case CREATE_RANK_ROLES:
                        new CreateRankRolesHandler().handle(event, client);
                        break;
                    case UPDATE_RANKS:
                        new UpdateRanksHandler().handle(event, client);
                        break;
                    case JOB_UPDATE_RANKS_START:
                        new JobUpdateRanksHandler().handle(event, client);
                        break;
                    case STATUS:
                        new StatusHandler().handle(event, client);
                        break;
                    default:
                        break;
                }
            }
        } catch (RateLimitException e) {
            Discord4J.LOGGER.error("Sending messages too quickly!", e);
        } catch (DiscordException e) {
            Discord4J.LOGGER.error(e.getErrorMessage(), e);
        } catch (MissingPermissionsException e) {
            Discord4J.LOGGER.error("Missing permissions for channel!", e);
        }
    }
}
