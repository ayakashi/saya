package com.devrine.listeners.handlers.impl.greetings;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.TextFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GreetingsResetHandler implements EventHandler {
    private static final Logger logger = LoggerFactory.getLogger(GreetingsResetHandler.class);

    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();

        DBManager.executeConfig(channel, configDao -> {
            if (configDao.isConfigInDatabase(guild)) {
                configDao.updateGreetings(guild, null);
            }

            TextFormatter.sendMessage(channel, "main.success", "main.greetings.changed");
        });
        logger.info("[{}] Greetings refresh finished", guild.getName());
    }
}
