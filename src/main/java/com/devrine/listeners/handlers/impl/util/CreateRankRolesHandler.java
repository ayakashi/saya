package com.devrine.listeners.handlers.impl.util;

import com.devrine.constants.enumerations.role.DiscordRole;
import com.devrine.constants.enumerations.role.GroupRankRole;
import com.devrine.constants.enumerations.role.RankRole;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.formatter.embed.TextFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;

import java.util.Arrays;
import java.util.List;


public class CreateRankRolesHandler implements EventHandler {
    private static final Logger logger = LoggerFactory.getLogger(CreateRankRolesHandler.class);

    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client){
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();

        createRoles(guild, GroupRankRole.values());
        createRoles(guild, RankRole.values());

        TextFormatter.sendMessage(channel, "main.success", "command.roles.created");
        logger.info("[{}] Rank roles creation finished", guild.getName());
    }

    private void createRoles(IGuild guild, DiscordRole[] roles) {
        Arrays.stream(roles)
                .sorted((role1, role2) -> role2.getValue() - role1.getValue())
                .forEach(rankRole -> {
                    List<IRole> existingRoles = guild.getRolesByName(rankRole.getName());
                    if (existingRoles == null || existingRoles.isEmpty()) {
                        IRole role = guild.createRole();
                        role.edit(
                                rankRole.getColor(),
                                role.isHoisted(),
                                rankRole.getName(),
                                role.getPermissions(),
                                true
                        );
                    } else {
                        existingRoles.forEach(role -> role.edit(
                                rankRole.getColor(),
                                role.isHoisted(),
                                role.getName(),
                                role.getPermissions(),
                                true
                        ));
                    }
                });
    }
}
