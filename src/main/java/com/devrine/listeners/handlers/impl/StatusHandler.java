package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;

import com.devrine.constants.enumerations.command.Command;
import com.devrine.constants.enumerations.Server;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.lolapi.status.beans.ShardStatus;
import com.devrine.lolapi.status.workers.StatusWorker;
import com.devrine.util.formatter.embed.EmbedFormatter;

public class StatusHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        Server server = null;
        String content = message.getContent().replace(Command.STATUS.getName(), "");
        if (content.startsWith("-")) {
            server = Server.getByName(content.substring(1));
        }
        if (server == null) {
            server = Server.RU;
        }

        ShardStatus status = StatusWorker.getShardData(server.getEndpoint());

        EmbedFormatter.sendApiStatus(event.getChannel(), status);
    }
}
