package com.devrine.listeners.handlers.impl;

import com.devrine.beans.Summoner;
import com.devrine.beans.User;
import com.devrine.constants.enumerations.Server;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.lolapi.summoner.beans.SummonerDto;
import com.devrine.lolapi.summoner.workers.SummonerWorker;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.ErrorFormatter;
import com.devrine.util.formatter.embed.TextFormatter;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.ArrayList;
import java.util.List;


public class UserAddHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();
        List<IUser> mentions = message.getMentions();
        if (mentions == null || mentions.size() != 1) {
            return;
        }

        Discord4J.LOGGER.info(message.getContent());

        IUser mention = mentions.get(0);
        User user = new User();
        user.setUserId(mention.getStringID());
        user.setDiscriminator(mention.getDiscriminator());
        user.setName(mention.getName());

        String content = message.getContent();
        content = content.replace(mention.mention(true), "");
        content = content.replace(mention.mention(false), "");
        content = content.trim();
        if (content.contains(" ")) {
            content = content.substring(content.indexOf(' ')).trim();
            SummonerDto summonerDto = SummonerWorker.summonerByName(Server.RU.getEndpoint(), content);
            if (summonerDto != null) {
                List<Summoner> summoners = new ArrayList<>();
                summoners.add(new Summoner(summonerDto.getPuuid(), summonerDto.getAccountId(), Server.RU, content));
                user.setSummoners(summoners);
            }
        }

        DBManager.executeUser(null, userDao -> {
            boolean isAdded;
            if (!userDao.isUserInDatabase(user)) {
                isAdded = userDao.addUser(user);
            } else {
                ErrorFormatter.sendErrorMessage(channel,"main.user.add.exists");
                return;
            }

            if (isAdded) {
                TextFormatter.sendMessage(channel, "main.success","main.user.add.success");
            } else {
                ErrorFormatter.sendErrorMessage(channel,"main.user.add.fail");
            }
        });
    }
}
