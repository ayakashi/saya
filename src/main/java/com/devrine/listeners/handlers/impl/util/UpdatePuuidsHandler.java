package com.devrine.listeners.handlers.impl.util;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.TextFormatter;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;


public class UpdatePuuidsHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();

        DBManager.executeSummoner(channel, summonerDao -> {
            if (summonerDao.updatePuuids()) {
                TextFormatter.sendMessage(channel, "main.success", "main.user.puuids.update.success");
            } else {
                TextFormatter.sendMessage(channel, "main.discord.error", "main.discord.error");
            }
        });
    }
}
