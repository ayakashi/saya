package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;

import com.devrine.constants.enumerations.command.Category;
import com.devrine.constants.enumerations.command.Command;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.formatter.embed.EmbedFormatter;

import org.apache.commons.lang3.StringUtils;

public class HelpHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();

        String content = message.getContent().replace(Command.HELP.getName(), "");
        if (StringUtils.isBlank(content)) {
            EmbedFormatter.sendCommandsList(channel, Command.getImplementedForUser(event));
        } else {
            Category category = Category.getByCode(content.substring(1).trim());
            if (category != null) {
                EmbedFormatter.sendCategoryCommands(channel, category, Command.getImplementedForUser(event));
            }
        }
    }
}
