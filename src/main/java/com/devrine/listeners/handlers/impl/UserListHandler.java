package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;

import java.util.List;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.formatter.embed.EmbedFormatter;
import com.devrine.util.formatter.embed.ErrorFormatter;


public class UserListHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();
        List<IRole> roles = message.getRoleMentions();

        if (roles != null && !roles.isEmpty()) {
            EmbedFormatter.sendListUsers(channel, guild.getUsersByRole(roles.get(0)));
        } else {
            ErrorFormatter.sendErrorMessage(channel,"main.discord.no.role");
        }
    }
}
