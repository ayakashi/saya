package com.devrine.listeners.handlers.impl;

import com.devrine.constants.enumerations.Language;
import com.devrine.util.db.DBManager;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.util.MessageBuilder;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.Properties;

public class PingHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guild.getStringID());
            new MessageBuilder(client)
                    .withChannel(channel)
                    .appendContent(
                            String.valueOf(channel.getShard().getResponseTime())
                                    + Properties.getLocalizedMessage(lang, "main.ms"),
                            MessageBuilder.Styles.INLINE_CODE)
                    .build();
        });
    }
}
