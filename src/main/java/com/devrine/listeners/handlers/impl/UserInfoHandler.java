package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.List;

import com.devrine.beans.User;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.EmbedFormatter;

public class UserInfoHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        List<IUser> mentions = message.getMentions();

        DBManager.executeUser(event.getChannel(), userDao -> {
            for (IUser mention : mentions) {
                User user = userDao.getUser(mention.getStringID());
                EmbedFormatter.sendUserInfo(event.getChannel(), mention, user, false);
            }
        });
    }
}
