package com.devrine.listeners.handlers.impl.util;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.EmbedFormatter;
import com.devrine.util.formatter.embed.TextFormatter;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.util.HashSet;
import java.util.Set;


public class UpdateIdsHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();

        DBManager.executeUser(channel, userDao -> {
            userDao.updateIds(guild);

            TextFormatter.sendMessage(channel, "main.success", "main.user.ids.update.success");

            Set<IUser> warnUsers = new HashSet<>();
            for (IUser user1 : guild.getUsers()) {
                for (IUser user2 : guild.getUsers()) {
                    if (user1.getLongID() != user2.getLongID() && user1.getDiscriminator().equals(user2.getDiscriminator())) {
                        warnUsers.add(user1);
                        warnUsers.add(user2);
                    }
                }
            }

            EmbedFormatter.sendUsersWithDuplicateDiscriminators(channel, warnUsers);
        });
    }
}
