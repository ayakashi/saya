package com.devrine.listeners.handlers.impl;

import com.devrine.beans.User;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.EmbedFormatter;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IUser;

import java.util.ArrayList;
import java.util.List;


public class UnknownHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();

        DBManager.executeUser(channel, userDao -> {
            List<User> users = userDao.getAllUsers();
            List<IUser> discordUsers = channel.getGuild().getUsers();
            List<IUser> notRegisteredDiscordUsers = new ArrayList<>();
            for (IUser discordUser : discordUsers) {
                if (!isUserRegistered(discordUser, users)) {
                    notRegisteredDiscordUsers.add(discordUser);
                }
            }

            EmbedFormatter.sendUnknownUsersInfo(channel, notRegisteredDiscordUsers);
        });
    }

    private boolean isUserRegistered(IUser discordUser, List<User> users) {
        if (discordUser != null) {
            if (discordUser.isBot()) {
                return true;
            }
            for (User user : users) {
                if (discordUser.getStringID().equals(user.getUserId())) {
                    return true;
                }
            }
        }
        return false;
    }
}
