package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IMessage;

import com.devrine.constants.enumerations.command.Command;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.JobWorker;


public class JobUpdateRanksHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();

        if (message.getContent().equals(Command.JOB_UPDATE_RANKS_START.getName())) {
            JobWorker.startUpdateDiscordRank(message.getChannel());
        } else if (message.getContent().equals(Command.JOB_UPDATE_RANKS_STOP.getName())) {
            JobWorker.stopUpdateDiscordRank();
        }
    }
}
