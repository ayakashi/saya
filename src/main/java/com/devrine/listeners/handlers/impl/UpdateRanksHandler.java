package com.devrine.listeners.handlers.impl;

import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.ErrorFormatter;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IRole;

import java.util.List;


public class UpdateRanksHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();
        List<IRole> mentions = message.getRoleMentions();

        if (mentions.size() != 1 && !message.getContent().contains("@everyone")) {
            ErrorFormatter.sendErrorMessage(channel,"main.discord.error");
            return;
        }

        new Thread(() -> DBManager.executeUser(channel, userDao -> userDao.updateDiscordRanks(client, channel, mentions.size() == 1 ? mentions.get(0) : null))).start();
    }
}
