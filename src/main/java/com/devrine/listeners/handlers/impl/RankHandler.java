package com.devrine.listeners.handlers.impl;

import com.devrine.beans.Summoner;
import com.devrine.beans.User;
import com.devrine.constants.enumerations.Language;
import com.devrine.constants.enumerations.Server;
import com.devrine.constants.enumerations.command.Command;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.Properties;
import com.devrine.util.RankUtil;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.EmbedFormatter;
import com.devrine.util.formatter.embed.ErrorFormatter;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.text.MessageFormat;
import java.util.List;


public class RankHandler implements EventHandler {
    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IMessage message = event.getMessage();
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();
        List<IUser> mentions = message.getMentions();

        DBManager.executeUser(channel, userDao -> {
            User user;
            Server server = null;
            String content = message.getContent().replace(Command.RANK.getName(), "");
            if (content.startsWith("-")) {
                String serverStr = content.substring(1, content.indexOf(' '));
                server = Server.getByName(serverStr);
                content = content.substring(content.indexOf(' '));
            }
            if (server == null) {
                server = Server.RU;
            }

            for (IUser mention : mentions) {
                content = content.replace(mention.mention(true), "");
                content = content.replace(mention.mention(false), "");

                user = userDao.getUser(mention.getStringID());
                if (user == null) {
                    DBManager.executeConfig(channel, configDao -> {
                        final Language lang = configDao.getLanguage(guild.getStringID());
                        final String unknown = MessageFormat.format(
                                Properties.getLocalizedMessage(lang, "main.discord.is.unknown"),
                                mention.getDisplayName(guild));
                        ErrorFormatter.sendCustomErrorMessage(channel, unknown);
                    });
                    continue;
                }
                for (Summoner summoner : user.getSummoners()) {
                    summoner.setRanks(RankUtil.getRanks(summoner.getServer(), summoner.getName()));
                }
                EmbedFormatter.sendUserInfo(channel, mention, user, false);
            }
            Summoner summoner;
            for (String nickname : content.split(",")) {
                nickname = nickname.trim();
                if (!nickname.isEmpty()) {
                    summoner = new Summoner();
                    summoner.setName(nickname);
                    summoner.setServer(server);
                    summoner.setRanks(RankUtil.getRanks(summoner.getServer(), summoner.getName()));

                    EmbedFormatter.sendSummonerInfo(channel, summoner);
                }
            }
        });
    }
}
