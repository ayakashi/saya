package com.devrine.listeners.handlers.impl;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;

import com.devrine.constants.enumerations.command.Command;
import com.devrine.constants.enumerations.Language;
import com.devrine.listeners.handlers.EventHandler;
import com.devrine.util.Properties;
import com.devrine.util.db.DBManager;
import com.devrine.util.formatter.embed.TextFormatter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LanguageSetHandler implements EventHandler {
    private static final Logger logger = LoggerFactory.getLogger(LanguageSetHandler.class);

    @Override
    public void handle(MessageReceivedEvent event, IDiscordClient client) {
        IChannel channel = event.getChannel();
        IGuild guild = event.getGuild();

        String content = event.getMessage().getContent().replace(Command.SET_LANGUAGE.getName(), "").trim();
        Language lang = Language.getByName(content);
        if(lang != null) {
            DBManager.executeConfig(channel, configDao -> {
                if (configDao.isConfigInDatabase(guild)) {
                    configDao.updateLanguage(guild, lang);
                } else {
                    configDao.addLanguage(guild, lang);
                }
                Properties.getAllLanguages(configDao);
                TextFormatter.sendMessage(channel, "main.success", "main.language.changed");
            });
            logger.info("[{}] Language set finished", guild.getName());
        } else {
            logger.info("[{}] Language was not changed because of no new value specified", guild.getName());
        }
    }
}
