package com.devrine.listeners.handlers;

import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

@FunctionalInterface
public interface EventHandler {
    void handle(MessageReceivedEvent event, IDiscordClient client);
}
