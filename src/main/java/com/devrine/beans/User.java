package com.devrine.beans;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String userId;
    private String discriminator;
    private String name;
    private String country;
    private String city;
    private List<Summoner> summoners;

    public String getUserId() {
        return userId;
    }
    public void setUserId(String userId) {
        this.userId = userId;
    }
    public String getDiscriminator() {
        return discriminator;
    }
    public void setDiscriminator(String discriminator) {
        this.discriminator = discriminator;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getCountry() {
        return country;
    }
    public void setCountry(String country) {
        this.country = country;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public List<Summoner> getSummoners() {
        return summoners;
    }
    public void setSummoners(List<Summoner> summoners) {
        this.summoners = summoners;
    }

    public User() {
        super();
        this.summoners = new ArrayList<>();
    }

    public User(String userId, String discriminator, String name, String country, String city) {
        this.userId = userId;
        this.discriminator = discriminator;
        this.name = name;
        this.country = country;
        this.city = city;
        this.summoners = new ArrayList<>();
    }

    public User(String userId, String discriminator, String name, String country, String city, List<Summoner> summoners) {
        this.userId = userId;
        this.discriminator = discriminator;
        this.name = name;
        this.country = country;
        this.city = city;
        this.summoners = summoners;
    }
}
