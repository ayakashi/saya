package com.devrine.beans;

import com.devrine.constants.enumerations.Server;

import java.util.ArrayList;
import java.util.List;


public class Summoner {
    private String puuid;
    private String accountId;
    private Server server;
    private String name;
    private List<Rank> ranks = new ArrayList<>();

    public String getPuuid() {
        return puuid;
    }
    public void setPuuid(final String puuid) {
        this.puuid = puuid;
    }
    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(final String accountId) {
        this.accountId = accountId;
    }
    public Server getServer() {
        return server;
    }
    public void setServer(Server server) {
        this.server = server;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<Rank> getRanks() {
        return ranks;
    }
    public void setRanks(List<Rank> ranks) {
        this.ranks = ranks;
    }

    public Summoner() {
    }

    public Summoner(String puuid, String accountId, Server server, String name) {
        this.puuid = puuid;
        this.accountId = accountId;
        this.server = server;
        this.name = name;
    }
}
