package com.devrine.beans;

import com.devrine.constants.enumerations.Queue;

public class Rank {
    private Queue queue;
    private String tier;
    private String division;
    private int leaguePoints;
    private int wins;
    private int losses;

    public Queue getQueue() {
        return queue;
    }

    public void setQueue(Queue queue) {
        this.queue = queue;
    }

    public String getTier() {
        return tier;
    }

    public void setTier(String tier) {
        this.tier = tier;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getLeaguePoints() {
        return leaguePoints;
    }

    public void setLeaguePoints(int leaguePoints) {
        this.leaguePoints = leaguePoints;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public int getLosses() {
        return losses;
    }

    public void setLosses(int losses) {
        this.losses = losses;
    }

    public Rank() {
    }

    public Rank(Queue queue, String tier, String division, int leaguePoints, int wins, int losses) {
        this.queue = queue;
        this.tier = tier;
        this.division = division;
        this.leaguePoints = leaguePoints;
        this.wins = wins;
        this.losses = losses;
    }
}
