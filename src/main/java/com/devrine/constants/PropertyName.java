package com.devrine.constants;

public class PropertyName {
    public static final String BASE_URL = "baseUrl";
    public static final String BOT_TOKEN = "token";
    public static final String DB_URL = "dbUrl";
    public static final String DB_USER = "dbUser";
    public static final String DB_PASS = "dbPass";
}
