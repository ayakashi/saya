package com.devrine.constants;

public class Emoji {
    public static final String CHECK_MARK = ":white_check_mark:";
    public static final String WARNING = ":warning:";
    public static final String RED_CIRCLE = ":red_circle:";
    public static final String IN_PROGRESS = ":wheelchair:";
    public static final String FINISHED = ":triangular_flag_on_post:";
}
