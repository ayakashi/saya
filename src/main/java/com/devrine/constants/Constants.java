package com.devrine.constants;

public class Constants {
    public static final String QUEUE_SOLO = "RANKED_SOLO_5x5";
    public static final String QUEUE_FLEX = "RANKED_FLEX_SR";
    public static final String QUEUE_TREE = "RANKED_FLEX_TT";
    public static final String QUEUE_UNKNOWN = "UNKNOWN";

    public static final String RANK_IRON = "Iron";
    public static final String RANK_IRON_1 = RANK_IRON + " I";
    public static final String RANK_IRON_2 = RANK_IRON + " II";
    public static final String RANK_IRON_3 = RANK_IRON + " III";
    public static final String RANK_IRON_4 = RANK_IRON + " IV";
    public static final String RANK_BRONZE = "Bronze";
    public static final String RANK_BRONZE_1 = RANK_BRONZE + " I";
    public static final String RANK_BRONZE_2 = RANK_BRONZE + " II";
    public static final String RANK_BRONZE_3 = RANK_BRONZE + " III";
    public static final String RANK_BRONZE_4 = RANK_BRONZE + " IV";
    public static final String RANK_SILVER = "Silver";
    public static final String RANK_SILVER_1 = RANK_SILVER + " I";
    public static final String RANK_SILVER_2 = RANK_SILVER + " II";
    public static final String RANK_SILVER_3 = RANK_SILVER + " III";
    public static final String RANK_SILVER_4 = RANK_SILVER + " IV";
    public static final String RANK_GOLD = "Gold";
    public static final String RANK_GOLD_1 = RANK_GOLD + " I";
    public static final String RANK_GOLD_2 = RANK_GOLD + " II";
    public static final String RANK_GOLD_3 = RANK_GOLD + " III";
    public static final String RANK_GOLD_4 = RANK_GOLD + " IV";
    public static final String RANK_PLATINUM = "Platinum";
    public static final String RANK_PLATINUM_1 = RANK_PLATINUM + " I";
    public static final String RANK_PLATINUM_2 = RANK_PLATINUM + " II";
    public static final String RANK_PLATINUM_3 = RANK_PLATINUM + " III";
    public static final String RANK_PLATINUM_4 = RANK_PLATINUM + " IV";
    public static final String RANK_DIAMOND = "Diamond";
    public static final String RANK_DIAMOND_1 = RANK_DIAMOND + " I";
    public static final String RANK_DIAMOND_2 = RANK_DIAMOND + " II";
    public static final String RANK_DIAMOND_3 = RANK_DIAMOND + " III";
    public static final String RANK_DIAMOND_4 = RANK_DIAMOND + " IV";
    public static final String RANK_MASTER = "Master";
    public static final String RANK_MASTER_1 = RANK_MASTER + " I";
    public static final String RANK_GRANDMASTER = "Grandmaster";
    public static final String RANK_GRANDMASTER_1 = RANK_GRANDMASTER + " I";
    public static final String RANK_CHALLENGER = "Challenger";
    public static final String RANK_CHALLENGER_1 = RANK_CHALLENGER + " I";
    public static final String RANK_UNRANKED = "Unranked";
    public static final String RANK_COLOR_IRON = "#7A3333";
    public static final String RANK_COLOR_BRONZE = "#9B5F1E";
    public static final String RANK_COLOR_SILVER = "#B4B4B4";
    public static final String RANK_COLOR_GOLD = "#FFCC00";
    public static final String RANK_COLOR_PLATINUM = "#339900";
    public static final String RANK_COLOR_DIAMOND = "#33CCFF";
    public static final String RANK_COLOR_MASTER = "#333399";
    public static final String RANK_COLOR_GRANDMASTER = "#E00000";
    public static final String RANK_COLOR_CHALLENGER = "#FFFF33";
    public static final String RANK_COLOR_UNRANKED = "#CCCCFF";

}
