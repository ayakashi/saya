package com.devrine.constants.enumerations.role;

import java.awt.*;

public interface DiscordRole {
    String getName();
    void setName(String name);
    Color getColor();
    void setColor(String color);
    int getValue();
    void setValue(int value);
}
