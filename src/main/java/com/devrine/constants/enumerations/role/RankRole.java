package com.devrine.constants.enumerations.role;

import java.awt.Color;

import static com.devrine.constants.Constants.*;

public enum RankRole implements DiscordRole {
    IRON_I(RANK_IRON_1, RANK_COLOR_IRON, 4),
    IRON_II(RANK_IRON_2, RANK_COLOR_IRON, 3),
    IRON_III(RANK_IRON_3, RANK_COLOR_IRON, 2),
    IRON_IV(RANK_IRON_4, RANK_COLOR_IRON, 1),

    BRONZE_I(RANK_BRONZE_1, RANK_COLOR_BRONZE, 8),
    BRONZE_II(RANK_BRONZE_2, RANK_COLOR_BRONZE, 7),
    BRONZE_III(RANK_BRONZE_3, RANK_COLOR_BRONZE, 6),
    BRONZE_IV(RANK_BRONZE_4, RANK_COLOR_BRONZE, 5),

    SILVER_I(RANK_SILVER_1, RANK_COLOR_SILVER, 12),
    SILVER_II(RANK_SILVER_2, RANK_COLOR_SILVER, 11),
    SILVER_III(RANK_SILVER_3, RANK_COLOR_SILVER, 10),
    SILVER_IV(RANK_SILVER_4, RANK_COLOR_SILVER, 9),

    GOLD_I(RANK_GOLD_1, RANK_COLOR_GOLD, 16),
    GOLD_II(RANK_GOLD_2, RANK_COLOR_GOLD, 15),
    GOLD_III(RANK_GOLD_3, RANK_COLOR_GOLD, 14),
    GOLD_IV(RANK_GOLD_4, RANK_COLOR_GOLD, 13),

    PLATINUM_I(RANK_PLATINUM_1, RANK_COLOR_PLATINUM, 20),
    PLATINUM_II(RANK_PLATINUM_2, RANK_COLOR_PLATINUM, 19),
    PLATINUM_III(RANK_PLATINUM_3, RANK_COLOR_PLATINUM, 18),
    PLATINUM_IV(RANK_PLATINUM_4, RANK_COLOR_PLATINUM, 17),

    DIAMOND_I(RANK_DIAMOND_1, RANK_COLOR_DIAMOND, 24),
    DIAMOND_II(RANK_DIAMOND_2, RANK_COLOR_DIAMOND, 23),
    DIAMOND_III(RANK_DIAMOND_3, RANK_COLOR_DIAMOND, 22),
    DIAMOND_IV(RANK_DIAMOND_4, RANK_COLOR_DIAMOND, 21),

    MASTER_I(RANK_MASTER_1, RANK_COLOR_MASTER, 25),

    GRANDMASTER_I(RANK_GRANDMASTER_1, RANK_COLOR_GRANDMASTER, 26),

    CHALLENGER_I(RANK_CHALLENGER_1, RANK_COLOR_CHALLENGER, 27),

    UNRANKED(RANK_UNRANKED, RANK_COLOR_UNRANKED, 0);

    private String name;
    private String color;
    private int value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return Color.decode(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    RankRole(String name, String color, int value) {
        this.name = name;
        this.color = color;
        this.value = value;
    }

    public static RankRole get(String rank) {
        RankRole result = UNRANKED;
        for (RankRole r : values()) {
            if (r.name.equalsIgnoreCase(rank)) {
                result = r;
                break;
            }
        }
        return result;
    }
}
