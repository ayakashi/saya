package com.devrine.constants.enumerations.role;

import com.devrine.constants.Constants;

import java.awt.*;


public enum GroupRankRole implements DiscordRole {
    IRON       (Constants.RANK_IRON,        Constants.RANK_COLOR_IRON,        1),
    BRONZE     (Constants.RANK_BRONZE,      Constants.RANK_COLOR_BRONZE,      2),
    SILVER     (Constants.RANK_SILVER,      Constants.RANK_COLOR_SILVER,      3),
    GOLD       (Constants.RANK_GOLD,        Constants.RANK_COLOR_GOLD,        4),
    PLATINUM   (Constants.RANK_PLATINUM,    Constants.RANK_COLOR_PLATINUM,    5),
    DIAMOND    (Constants.RANK_DIAMOND,     Constants.RANK_COLOR_DIAMOND,     6),
    MASTER     (Constants.RANK_MASTER,      Constants.RANK_COLOR_MASTER,      7),
    GRANDMASTER(Constants.RANK_GRANDMASTER, Constants.RANK_COLOR_GRANDMASTER, 8),
    CHALLENGER (Constants.RANK_CHALLENGER,  Constants.RANK_COLOR_CHALLENGER,  9);

    private String name;
    private String color;
    private int value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Color getColor() {
        return Color.decode(color);
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    GroupRankRole(String name, String color, int value) {
        this.name = name;
        this.color = color;
        this.value = value;
    }

    public static GroupRankRole get(RankRole rank) {
        String rankName = rank.getName();
        if (RankRole.UNRANKED.equals(rank)) {
            return null;
        }
        return get(rankName.substring(0, rankName.indexOf(' ')));
    }

    public static GroupRankRole get(String rank) {
        GroupRankRole result = null;
        for (GroupRankRole r : values()) {
            if (r.name.equalsIgnoreCase(rank)) {
                result = r;
                break;
            }
        }
        return result;
    }
}
