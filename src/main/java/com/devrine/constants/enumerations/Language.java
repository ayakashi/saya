package com.devrine.constants.enumerations;

public enum Language {
    EN("en"),
    RU("ru");

    private String name;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    Language(String name) {
        this.name = name;
    }

    public static Language getByName(String name) {
        for(Language lang : values()) {
            if(lang.getName().equalsIgnoreCase(name)) {
                return lang;
            }
        }
        return null;
    }
}
