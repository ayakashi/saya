package com.devrine.constants.enumerations.command;

import com.devrine.constants.enumerations.Language;
import com.devrine.constants.enumerations.UserRole;
import com.devrine.constants.localized.CBase;
import com.devrine.util.Properties;
import com.devrine.util.permissions.PermissionsUtil;
import org.apache.commons.lang3.StringUtils;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;


public enum Command {
    //all
    PING("--ping",
            true,
            Category.INFO,
            UserRole.GUEST,
            CBase.COMMAND_HELP_PING),
    HELP("--help", "[-{category}]",
            true,
            Category.INFO,
            UserRole.GUEST,
            CBase.COMMAND_HELP_HELP),
    //user
    RANK("--rank", "[-{server}] {summoner}",
            true,
            Category.LEAGUE,
            UserRole.USER,
            CBase.COMMAND_HELP_RANK),
    LIST("--list", " @{discord_role}",
            true,
            Category.INFO,
            UserRole.USER,
            CBase.COMMAND_HELP_LIST),
    STATUS("--status",
            true,
            Category.LEAGUE,
            UserRole.USER,
            CBase.COMMAND_HELP_STATUS),
    ONLINE("--online",
            false,
            Category.LEAGUE,
            UserRole.USER,
            CBase.COMMAND_HELP_ONLINE),
    UPCOMING("--upcoming",
            false,
            Category.EVENT,
            UserRole.USER,
            CBase.COMMAND_HELP_UPCOMING),
    //manager
    UNKNOWN("--unknown",
            true,
            Category.INFO,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_UNKNOWN),
    SET_LANGUAGE("--language-set", " {language}",
            true,
            Category.CONFIG,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_LANGUAGE_SET),
    SET_GREETINGS("--greetings-set", " {message}",
            true,
            Category.CONFIG,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_GREETINGS_SET),
    RESET_GREETINGS("--greetings-reset",
            true,
            Category.CONFIG,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_GREETINGS_RESET),
    ADD_USER("--add-user", " @{discord_user} {summoner_on_ru_server}",
            true,
            Category.CONFIG,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_USER_ADD),
    USER_INFO("--info",
            false,
            Category.INFO,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_USER_INFO),
    //admin
    CREATE_RANK_ROLES("--create-rank-roles",
            true,
            Category.UTIL,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_CREATE_RANK_ROLES),
    UPDATE_IDS("--update-ids",
            false,
            Category.UTIL,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_UPDATE_IDS),
    UPDATE_PUUIDS("--update-puuids",
            false,
            Category.UTIL,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_UPDATE_PUUIDS),
    UPDATE_RANKS("--update-ranks", " @{discord_role}",
            true,
            Category.LEAGUE,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_UPDATE_RANKS),
    JOB_UPDATE_RANKS_START("--job-update-ranks-start",
            true,
            Category.LEAGUE,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_JOB_UPDATE_RANKS_START),
    JOB_UPDATE_RANKS_STOP("--job-update-ranks-stop",
            true,
            Category.LEAGUE,
            UserRole.MANAGER,
            CBase.COMMAND_HELP_JOB_UPDATE_RANKS_STOP);

    private String name;
    private String examplePart;
    private boolean isImplemented;
    private Category category;
    private UserRole minUserRole;
    private String help;

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getExamplePart() {
        return examplePart;
    }
    public void setExamplePart(String examplePart) {
        this.examplePart = examplePart;
    }
    public boolean isImplemented() {
        return isImplemented;
    }
    public void setImplemented(boolean implemented) {
        isImplemented = implemented;
    }
    public Category getCategory() {
        return category;
    }
    public void setCategory(Category category) {
        this.category = category;
    }
    public UserRole getMinUserRole() {
        return minUserRole;
    }
    public void setMinUserRole(UserRole minUserRole) {
        this.minUserRole = minUserRole;
    }
    public String getHelp() {
        return Properties.getLocalizedMessage(null, help);
    }
    public String getHelp(Language language) {
        return Properties.getLocalizedMessage(language, help);
    }
    public void setHelp(String help) {
        this.help = help;
    }

    Command(String name, boolean isImplemented, Category category, UserRole minUserRole, String help) {
        this.name = name;
        this.examplePart = StringUtils.EMPTY;
        this.isImplemented = isImplemented;
        this.category = category;
        this.minUserRole = minUserRole;
        this.help = help;
    }

    Command(String name, String examplePart, boolean isImplemented, Category category, UserRole minUserRole, String help) {
        this.name = name;
        this.examplePart = examplePart;
        this.isImplemented = isImplemented;
        this.category = category;
        this.minUserRole = minUserRole;
        this.help = help;
    }

    public static Command getByName(String name) {
        Command result = null;
        if(name != null) {
            for (Command cmd : Command.values()) {
                if (name.startsWith(cmd.getName())) {
                    result = cmd;
                    break;
                }
            }
        }
        return result;
    }

    public static List<Command> getImplemented() {
        List<Command> result = new ArrayList<>();
        for(Command command : Command.values()) {
            if(command.isImplemented()) {
                result.add(command);
            }
        }
        return result;
    }

    public static List<Command> getImplementedForUser(MessageReceivedEvent event) {
        List<Command> result = new ArrayList<>();
        for(Command command : Command.values()) {
            if(command.isImplemented()
                  && PermissionsUtil.isAllowed(command, event.getAuthor().getPermissionsForGuild(event.getGuild()))) {
                result.add(command);
            }
        }
        return result;
    }

    public static int getMaxImplementedNameLength() {
        int result = 0;
        int curr;
        List<Command> implementedCommands = Command.getImplemented();
        if (!implementedCommands.isEmpty()) {
            result = implementedCommands.get(0).getName().length();
            for (Command command : implementedCommands) {
                curr = command.getName().length();
                if (curr > result) {
                    result = curr;
                }
            }
        }
        return result;
    }

    public static int getMaxImplementedHelpLength(Language language) {
        int result = 0;
        int curr;
        List<Command> implementedCommands = Command.getImplemented();
        if (!implementedCommands.isEmpty()) {
            result = implementedCommands.get(0).getHelp(language).length();
            for (Command command : implementedCommands) {
                curr = command.getName().length();
                if (curr > result) {
                    result = curr;
                }
            }
        }
        return result;
    }
}
