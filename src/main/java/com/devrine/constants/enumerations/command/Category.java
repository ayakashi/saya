package com.devrine.constants.enumerations.command;

import com.devrine.constants.localized.CBase;

public enum Category {
    INFO   ("info",    CBase.COMMAND_CATEGORY_INFO,    CBase.COMMAND_CATEGORY_HELP_INFO,    1),
    LEAGUE ("league",  CBase.COMMAND_CATEGORY_LEAGUE,  CBase.COMMAND_CATEGORY_HELP_LEAGUE,  2),
    EVENT  ("event",   CBase.COMMAND_CATEGORY_EVENT,   CBase.COMMAND_CATEGORY_HELP_EVENT,   3),
    CONFIG ("config",  CBase.COMMAND_CATEGORY_CONFIG,  CBase.COMMAND_CATEGORY_HELP_CONFIG,  4),
    UTIL   ("util",    CBase.COMMAND_CATEGORY_UTIL,    CBase.COMMAND_CATEGORY_HELP_UTIL,    5),
    DEFAULT("default", CBase.COMMAND_CATEGORY_DEFAULT, CBase.COMMAND_CATEGORY_HELP_DEFAULT, 100);

    private String code;
    private String name;
    private String help;
    private int priority;

    public String getCode() {
        return code;
    }
    public void setCode(final String code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(final String name) {
        this.name = name;
    }
    public String getHelp() {
        return help;
    }
    public void setHelp(final String help) {
        this.help = help;
    }
    public int getPriority() {
        return priority;
    }
    public void setPriority(final int priority) {
        this.priority = priority;
    }

    Category(final String code, final String name, String help, int priority) {
        this.code = code;
        this.name = name;
        this.help = help;
        this.priority = priority;
    }

    public static Category getByCode(String code) {
        Category result = null;
        if(code != null) {
            for (Category category : values()) {
                if (code.startsWith(category.getCode())) {
                    result = category;
                    break;
                }
            }
        }
        return result;
    }
}
