package com.devrine.constants.enumerations;

public enum UserRole {
	GUEST(0, "Guest"),
	USER(1, "User"),
	USER_1(2, "User1"),
	USER_2(3, "User2"),
	MANAGER(4, "Manager"),
	ADMIN(5, "Admin");

	private int id;
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	UserRole() {
	}

	UserRole(int id, String name) {
		this.id = id;
		this.name = name;
	}
}
