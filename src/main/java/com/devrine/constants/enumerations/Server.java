package com.devrine.constants.enumerations;

public enum Server {
	BR("BR", "BR", "BR1"),
	EUNE("EUNE", "EUNE", "EUN1"),
	EUW("EUW", "EUW", "EUW1"),
	JP("JP", "JP", "JP1"),
	KR("KR", "WWW", "KR"),//-_-//
	LAN("LAN", "LAN", "LA1"),
	LAS("LAS", "LAS", "LA2"),
	NA("NA", "NA", "NA1"),
	OCE("OCE", "OCE", "OC1"),
	TR("TR", "TR", "TR1"),
	RU("RU", "RU", "RU"),
	PBE("PBE", "", "PBE1");

	private String name;
	private String opGG;
	private String endpoint;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpGG() {
		return opGG;
	}

	public void setOpGG(String opGG) {
		this.opGG = opGG;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	Server() {
	}

	Server(String name, String opGG, String endpoint) {
		this.name = name;
		this.opGG = opGG;
		this.endpoint = endpoint;
	}

	public static Server getByName(String name) {
		for(Server lang : values()) {
			if(lang.getName().equalsIgnoreCase(name)) {
				return lang;
			}
		}
		return RU;
	}
}
