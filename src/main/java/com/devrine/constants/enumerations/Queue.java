package com.devrine.constants.enumerations;

import com.devrine.constants.localized.CQueue;
import com.devrine.constants.Constants;
import com.devrine.util.Properties;

public enum Queue {
    RANKED_SOLO_5x5(Constants.QUEUE_SOLO, CQueue.SOLO),
    RANKED_FLEX_SR(Constants.QUEUE_FLEX, CQueue.FLEX),
    RANKED_FLEX_TT(Constants.QUEUE_TREE, CQueue.TREE),
    UNKNOWN(Constants.QUEUE_UNKNOWN, CQueue.UNKNOWN);

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public String getName() {
        return Properties.getLocalizedMessage(null, name);
    }
    public String getName(Language language) {
        return Properties.getLocalizedMessage(language, name);
    }

    Queue(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static Queue getByCode(String code) {
        Queue result = null;
        if(code != null) {
            for (Queue queue : Queue.values()) {
                if (code.equals(queue.getCode())) {
                    result = queue;
                    break;
                }
            }
        }
        return result;
    }
}
