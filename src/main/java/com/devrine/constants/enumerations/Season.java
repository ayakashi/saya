package com.devrine.constants.enumerations;

public enum Season {
	PRESEASON3(0),
	SEASON3(1),
	PRESEASON2014(2),
	SEASON2014(3),
	PRESEASON2015(4),
	SEASON2015(5),
	PRESEASON2016(6),
	SEASON2016(7),
	PRESEASON2017(8),
	SEASON2017(9),
	PRESEASON2018(10),
	SEASON2018(11);

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	Season() {
	}

	Season(int id) {
		this.id = id;
	}
}
