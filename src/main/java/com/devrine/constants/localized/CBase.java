package com.devrine.constants.localized;

public class CBase {
    public static final String COMMAND_HELP_PING = "command.ping";
    public static final String COMMAND_HELP_HELP = "command.help";
    public static final String COMMAND_HELP_RANK = "command.rank";
    public static final String COMMAND_HELP_LIST = "command.list.users";
    public static final String COMMAND_HELP_STATUS = "command.status";
    public static final String COMMAND_HELP_ONLINE = "command.online";
    public static final String COMMAND_HELP_UPCOMING = "command.upcoming";
    public static final String COMMAND_HELP_UNKNOWN = "command.unknown";
    public static final String COMMAND_HELP_LANGUAGE_SET = "command.language.set";
    public static final String COMMAND_HELP_GREETINGS_SET = "command.greetings.set";
    public static final String COMMAND_HELP_GREETINGS_RESET = "command.greetings.reset";
    public static final String COMMAND_HELP_USER_ADD = "command.user.add";
    public static final String COMMAND_HELP_USER_INFO = "command.info";
    public static final String COMMAND_HELP_CREATE_RANK_ROLES = "command.create.rank.roles";
    public static final String COMMAND_HELP_UPDATE_IDS = "command.update.id";
    public static final String COMMAND_HELP_UPDATE_PUUIDS = "command.update.puuid";
    public static final String COMMAND_HELP_UPDATE_RANKS = "command.update.ranks";
    public static final String COMMAND_HELP_JOB_UPDATE_RANKS_START = "command.job.update.ranks.start";
    public static final String COMMAND_HELP_JOB_UPDATE_RANKS_STOP = "command.job.update.ranks.stop";

    public static final String COMMAND_CATEGORY_INFO = "command.category.info";
    public static final String COMMAND_CATEGORY_LEAGUE = "command.category.league";
    public static final String COMMAND_CATEGORY_EVENT = "command.category.event";
    public static final String COMMAND_CATEGORY_CONFIG = "command.category.config";
    public static final String COMMAND_CATEGORY_UTIL = "command.category.util";
    public static final String COMMAND_CATEGORY_DEFAULT = "command.category.default";
    public static final String COMMAND_CATEGORY_HELP_INFO = "command.category.help.info";
    public static final String COMMAND_CATEGORY_HELP_LEAGUE = "command.category.help.league";
    public static final String COMMAND_CATEGORY_HELP_EVENT = "command.category.help.event";
    public static final String COMMAND_CATEGORY_HELP_CONFIG = "command.category.help.config";
    public static final String COMMAND_CATEGORY_HELP_UTIL = "command.category.help.util";
    public static final String COMMAND_CATEGORY_HELP_DEFAULT = "command.category.help.default";
}
