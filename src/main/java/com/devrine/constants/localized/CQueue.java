package com.devrine.constants.localized;

public class CQueue {
    public static final String SOLO = "queue.solo";
    public static final String FLEX = "queue.flex";
    public static final String TREE = "queue.tree";
    public static final String UNKNOWN = "queue.unknown";

}
