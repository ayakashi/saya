package com.devrine.constants;

public class Database {
	public static final String CONNECTION_FAILED = "Connection to database failed!";
	public static final String CONNECTION_IS_NOT_READY = "Connection is not ready.";

	public static final String SELECT_ALL_SUMMONERS =
			"select summoner.puuid as puuid, " +
					"summoner.accountId as accountId, " +
					"summoner.server as server, " +
					"summoner.name as name " +
					"from summoner as summoner";
	public static final String SELECT_ALL_USERS =
			"select user.user_id as user_id, " +
					"user.discriminator as discriminator, " +
					"user.name as name " +
			"from user as user";
	public static final String SELECT_ALL_LANGUAGES =
			"select config.guild_id as guild_id, config.language as language from config as config";
	public static final String SELECT_LANGUAGE =
			"select config.language as language from config as config where config.guild_id=?";
	public static final String SELECT_GREETINGS =
			"select config.welcome_message as welcome_message from config as config where config.guild_id=?";
	public static final String SELECT_USER =
			"select user.user_id as user_id, " +
					"user.discriminator as discriminator, " +
					"user.name as name, " +
					"user.country as country, " +
					"user.city as city, " +
					"summoner.puuid as puuid, " +
					"summoner.accountId as accountId, " +
					"summoner.server as server, " +
					"summoner.name as summonerName " +
			"from user as user, " +
					"summoner as summoner " +
			"where summoner.userId=user.id and user.user_id=? " +
			"order by 2";

	public static final String INSERT_LANGUAGE = "INSERT INTO config (guild_id, guild_name, language) VALUES (?,?,?)";
	public static final String INSERT_GREETINGS = "INSERT INTO config (guild_id, guild_name, welcome_message) VALUES (?,?,?)";
	public static final String INSERT_USER = "INSERT INTO user (user_id, discriminator, name, country, city) VALUES (?,?,?,?,?)";
	public static final String INSERT_SUMMONER = "INSERT INTO summoner (userId, puuid, accountId, server, name) SELECT u.id,?,?,?,? FROM user as u WHERE u.user_id = ?";

	public static final int MAX_CONNECTIONS = 150;
}
