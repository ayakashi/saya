package com.devrine.util;

import sx.blah.discord.handle.obj.IChannel;

import com.devrine.jobs.UpdateDiscordRank;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class JobWorker {
	private static final Logger logger = LoggerFactory.getLogger(JobWorker.class);
	private static Scheduler scheduler = null;

	public static void startUpdateDiscordRank(IChannel channel) {
		SchedulerFactory sf = new StdSchedulerFactory();
		try {
			if(!scheduler.isStarted()) {
				scheduler = sf.getScheduler();
				JobDetail job = JobBuilder.newJob(UpdateDiscordRank.class)
						.withIdentity("updateDiscordRank for " + channel.getStringID(), "groupApi")
						.usingJobData("channel", channel.getLongID())
						.build();

				Trigger trigger = TriggerBuilder.newTrigger()
						.withIdentity("triggerDaily for " + channel.getStringID(), "groupApi")
						.withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(24).repeatForever())
						.build();

				scheduler.scheduleJob(job, trigger);

				scheduler.start();
			}
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public static void stopUpdateDiscordRank() {
		try {
			if (scheduler != null && scheduler.isStarted()) {
				scheduler.interrupt(JobKey.jobKey("updateDiscordRank", "groupApi"));
				scheduler.unscheduleJob(TriggerKey.triggerKey("triggerDaily", "groupApi"));
			}
		} catch (SchedulerException e) {
			logger.error(e.getMessage(), e);
		}
	}
}
