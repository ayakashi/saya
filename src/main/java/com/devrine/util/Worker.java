package com.devrine.util;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RateLimitException;

import com.devrine.constants.PropertyName;
import com.devrine.listeners.EventListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Worker {
    private static final Logger logger = LoggerFactory.getLogger(Worker.class);
    private static IDiscordClient client;

    public static void start() {
        final String token = Properties.getProperty(PropertyName.BOT_TOKEN);

        client = Worker.getClient(token, true);
        if (client != null) {
            EventListener.setClient(client);
            EventDispatcher dispatcher = client.getDispatcher();
            dispatcher.registerListener(new EventListener());
        }

        com.devrine.lolapi.workers.Worker.setBaseUrl(Properties.getProperty(PropertyName.BASE_URL));
    }

    public static IDiscordClient getClient() {
        return client;
    }

    public static IDiscordClient getClient(String token, boolean login) {
        IDiscordClient client = null;
        ClientBuilder clientBuilder = new ClientBuilder();
        clientBuilder.withToken(token).withRecommendedShardCount();
        if (login) {
            try {
                client = clientBuilder.login();
            } catch (DiscordException e) {
                logger.error(e.getMessage(), e);
            }
        } else {
            try {
                client = clientBuilder.build();
            } catch (DiscordException e) {
                logger.error(e.getMessage(), e);
            }
        }
        return client;
    }

    public static void stop() {
        try {
            client.logout();
        } catch (RateLimitException e) {
            logger.error(e.getMessage(), e);
        } catch (DiscordException e) {
            logger.error(e.getMessage(), e);
        }
    }
}
