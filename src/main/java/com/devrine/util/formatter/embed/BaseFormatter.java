package com.devrine.util.formatter.embed;

import sx.blah.discord.util.EmbedBuilder;

import java.awt.Color;

public class BaseFormatter {
    public static final String BR = "\n";

    public static final Color COLOR_SUCCESS = Color.GREEN;
    public static final Color COLOR_INFO = Color.CYAN;
    public static final Color COLOR_WARNING = Color.ORANGE;
    public static final Color COLOR_ERROR = Color.RED;

    public static EmbedBuilder createBuilder() {
        return new EmbedBuilder().withColor(COLOR_SUCCESS);
    }
}
