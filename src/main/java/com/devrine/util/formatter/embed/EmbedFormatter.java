package com.devrine.util.formatter.embed;

import com.devrine.beans.Rank;
import com.devrine.beans.Summoner;
import com.devrine.beans.User;
import com.devrine.constants.Emoji;
import com.devrine.constants.enumerations.Language;
import com.devrine.constants.enumerations.command.Category;
import com.devrine.constants.enumerations.command.Command;
import com.devrine.lolapi.status.beans.Incident;
import com.devrine.lolapi.status.beans.Message;
import com.devrine.lolapi.status.beans.Service;
import com.devrine.lolapi.status.beans.ShardStatus;
import com.devrine.util.db.DBManager;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.devrine.util.Properties.getLocalizedMessage;
import static com.devrine.util.formatter.embed.BaseFormatter.*;

public class EmbedFormatter {
    public static void sendUserInfo(IChannel channel, IUser discordUser, User user, boolean full) {
        IGuild guild = channel.getGuild();
        EmbedBuilder builder = createBuilder()
                .withAuthorName(discordUser.getDisplayName(guild))
                .withAuthorIcon(discordUser.getAvatarURL());

        if (full) {
            formatPersonalData(builder, user, channel);
        }

        for (Summoner summoner : user.getSummoners()) {
            formatSummoner(builder, summoner, channel);
        }
        channel.sendMessage(builder.build());
    }

    public static void sendSummonerInfo(IChannel channel, Summoner summoner) {
        channel.sendMessage(formatSummoner(createBuilder(), summoner, channel).build());
    }

    public static void sendDefaultGreetings(IChannel channel, IUser user) {
        String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            channel.sendMessage(
                    createBuilder().appendField(
                            getLocalizedMessage(lang, "message.greetings.title.static"),
                            MessageFormat.format(
                                    getLocalizedMessage(lang, "message.greetings.static"),
                                    user.mention()
                            ),
                            false
                    ).withColor(COLOR_INFO).build()
            );
        });
    }

    public static void sendGreetings(IChannel channel, String greetings, IUser user) {
        String guildId = channel.getGuild().getStringID();
        String greetingsFormatted = greetings.replaceAll("\\{user}", user.mention());
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            channel.sendMessage(
                    createBuilder().appendField(
                            getLocalizedMessage(lang, "message.greetings.title.static"),
                            greetingsFormatted,
                            false
                    ).withColor(COLOR_INFO).build()
            );
        });
    }

    //==========================UNKNOWN=================================================================================
    public static void sendUnknownUsersInfo(IChannel channel, List<IUser> users) {
        EmbedBuilder builder = createBuilder().withColor(COLOR_INFO);
        String guildId = channel.getGuild().getStringID();
        StringBuilder sb = new StringBuilder();
        for (IUser user : users) {
            if (sb.length() + user.mention().length() <= 1024) {
                sb.append(user.mention()).append(BR);
            }
        }

        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            if (sb.length() > 0) {
                builder.appendField(
                        getLocalizedMessage(lang, "main.unknown.users") + ": ",
                        sb.toString(),
                        false
                );
            } else {
                builder.appendField(
                        getLocalizedMessage(lang, "main.unknown.users") + ": ",
                        getLocalizedMessage(lang, "main.no.unknown.users"),
                        false
                );
            }
        });
        channel.sendMessage(builder.build());
    }
    //==========================UNKNOWN=================================================================================
    //==========================LIST====================================================================================
    public static void sendListUsers(IChannel channel, List<IUser> users) {
        StringBuilder sb = new StringBuilder();
        for (IUser user : users) {
            sb.append(user.mention()).append(BR);
        }

        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(channel.getGuild().getStringID());
            channel.sendMessage(createBuilder().appendField(
                    getLocalizedMessage(lang, "main.list.users") + " (" + users.size() + "): ",
                    sb.toString(),
                    false
            ).withColor(COLOR_INFO).build());
        });
    }
    //==========================LIST====================================================================================
    //==========================DUPLICATE DISCRIMINANTS=================================================================
    public static void sendUsersWithDuplicateDiscriminators(IChannel channel, Set<IUser> users) {
        StringBuilder sb = new StringBuilder();
        for (IUser user : users) {
            sb.append(user.mention()).append(BR);
        }

        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(channel.getGuild().getStringID());
            channel.sendMessage(createBuilder().appendField(
                    getLocalizedMessage(lang,
                            "main.list.users.discriminators.warn") + " (" + users.size() + "): ",
                    sb.toString(),
                    false
            ).withColor(COLOR_WARNING).build());
        });
    }
    //==========================DUPLICATE DISCRIMINANTS=================================================================
    //==========================API_STATUS==============================================================================
    public static void sendApiStatus(IChannel channel, ShardStatus status) {
        boolean isOk = true;
        EmbedBuilder builder = createBuilder();
        builder.withTitle(status.getName());
        for (Service service : status.getServices()) {
            builder.appendField(
                    service.getName(),
                    "online".equals(service.getStatus()) ? Emoji.CHECK_MARK : service.getStatus(),
                    false
            );

            if (!"online".equals(service.getStatus())) {
                isOk = false;
            }

            for (Incident incident : service.getIncidents()) {
                StringBuilder sb = new StringBuilder();
                for (Message message : incident.getUpdates()) {
                    switch (message.getSeverity()) {
                        case "info":
                            sb.append(Emoji.WARNING);
                            break;
                        case "error":
                            sb.append(Emoji.RED_CIRCLE);
                            break;
                        default:
                            sb.append(message.getSeverity());
                    }
                    sb.append(BR);
                    sb.append(message.getContent());
                }
                builder.appendField(
                        incident.isActive() ? Emoji.IN_PROGRESS : Emoji.FINISHED,
                        sb.toString(),
                        false
                );
            }
        }

        builder.withColor(isOk ? COLOR_SUCCESS : COLOR_WARNING);
        channel.sendMessage(builder.build());
    }
    //==========================API_STATUS==============================================================================
    //==========================HELP====================================================================================
    public static void sendCommandsList(IChannel channel, List<Command> commands) {
        String guildId = channel.getGuild().getStringID();
        EmbedBuilder builder = createBuilder();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            builder.withTitle(getLocalizedMessage(lang, "main.list.commands"));
        });
        builder.withColor(COLOR_INFO);

        Map<Category, List<Command>> categories = new HashMap<>(Category.values().length);

        commands.stream()
                .sorted((c1, c2) -> c2.getCategory().getPriority() - c1.getCategory().getPriority())
                .forEach(c -> {
                    List<Command> cmds1 = categories.get(c.getCategory());
                    if (cmds1 != null) {
                        cmds1.add(c);
                    } else {
                        List<Command> cmds2 = new ArrayList<>();
                        cmds2.add(c);
                        categories.put(c.getCategory(), cmds2);
                    }
        });

        categories.forEach((category, cCommands) -> formatCategory(channel, builder, category, cCommands));
        channel.sendMessage(builder.build());
    }

    public static void sendCategoryCommands(IChannel channel, Category category, List<Command> commands) {
        if (category == null) {
            return;
        }

        String guildId = channel.getGuild().getStringID();
        EmbedBuilder builder = createBuilder();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            builder.withTitle(getLocalizedMessage(lang, "main.list.commands"));
        });
        builder.withColor(COLOR_INFO);

        List<Command> commandsFromCategory = commands.stream()
                .filter(c -> category.equals(c.getCategory()))
                .collect(Collectors.toList());
        formatCategory(channel, builder, category, commandsFromCategory);

        channel.sendMessage(builder.build());
    }

    public static void formatCategory(IChannel channel, EmbedBuilder builder, Category category, List<Command> commands) {
        String guildId = channel.getGuild().getStringID();
        StringBuilder sb = new StringBuilder();

        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            commands.forEach(command -> {
                sb.append("```");
                sb.append(command.getName()).append(command.getExamplePart());
                sb.append("```");
                sb.append(command.getHelp(lang)).append(BR).append(BR);
            });

            builder.appendField(
                    BR + getLocalizedMessage(lang, category.getName()),
                    sb.toString(),
                    false);
        });
    }
    //==========================HELP====================================================================================

    public static EmbedBuilder formatPersonalData(EmbedBuilder builder, User user, IChannel channel) {
        final String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            final String title = getLocalizedMessage(lang, "main.discord.info") + ":";
            final String country = getLocalizedMessage(lang, "main.discord.country") + ": ";
            final String city = getLocalizedMessage(lang, "main.discord.city") + " : ";
            builder.appendField(title, country + user.getCountry() + BR + city + user.getCity(), false);
        });
        return builder;
    }

    public static EmbedBuilder formatSummoner(EmbedBuilder builder, Summoner summoner, IChannel channel) {
        final String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            if (summoner.getRanks() == null || summoner.getRanks().isEmpty()) {
                final String cantFind = MessageFormat.format(
                        getLocalizedMessage(lang, "errors.discord.could.not.find.rank.for.summoner"),
                        summoner.getName(), summoner.getServer().getName().toLowerCase()
                );
                builder.appendField(
                        getLocalizedMessage(lang, "main.discord.error") + ":",
                        cantFind,
                        false
                ).withColor(COLOR_ERROR);
            } else {
                final String title = getLocalizedMessage(lang, "main.summoner") + ":";
                final String name = getLocalizedMessage(lang, "main.in.game.name") + ": ";
                final String server = getLocalizedMessage(lang, "main.server") + "        : ";
                builder.appendField(
                        title,
                        name + summoner.getName() + BR +
                                server + summoner.getServer().getName() + BR +
                                "https://" + summoner.getServer().getOpGG() + ".op.gg/summoner/userName=" + summoner.getName().replace(" ", ""),
                        false
                );
                formatRanks(builder, summoner.getRanks(), channel);
            }
        });
        return builder;
    }

    public static void formatRanks(EmbedBuilder builder, List<Rank> ranks, IChannel channel) {
        for (Rank rank : ranks) {
            formatRank(builder, rank, channel);
        }
    }

    public static void formatRank(EmbedBuilder builder, Rank rank, IChannel channel) {
        final String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            final String lp = getLocalizedMessage(lang, "main.league.points");
            final String wins = getLocalizedMessage(lang, "main.wins") + "          : ";
            final String loses = getLocalizedMessage(lang, "main.loses") + ": ";

            builder.appendField(
                    rank.getQueue().getName(lang),
                    rank.getTier() + " " + rank.getDivision() + BR +
                            rank.getLeaguePoints() + lp + BR +
                            wins + rank.getWins() + BR +
                            loses + rank.getLosses(), true);
        });
    }
}
