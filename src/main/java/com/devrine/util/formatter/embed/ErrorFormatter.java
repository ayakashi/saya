package com.devrine.util.formatter.embed;

import sx.blah.discord.handle.obj.IChannel;

import java.awt.Color;

import static com.devrine.util.Properties.getLocalizedMessage;
import static com.devrine.util.formatter.embed.BaseFormatter.createBuilder;

public class ErrorFormatter {
    public static void sendErrorMessage(IChannel channel, String text) {
        channel.sendMessage(
                createBuilder().appendField(
                        getLocalizedMessage("main.discord.error") + ":",
                        getLocalizedMessage(text),
                        false
                ).withColor(Color.RED).build()
        );
    }

    public static void sendCustomErrorMessage(IChannel channel, String text) {
        channel.sendMessage(
                createBuilder().appendField(
                        getLocalizedMessage("main.discord.error") + ":",
                        text,
                        false
                ).withColor(Color.RED).build()
        );
    }

    public static void sendDBErrorTooManyConnections(IChannel channel) {
        sendErrorMessage(
                channel,
                getLocalizedMessage("error.db.too.many.connections")
        );
    }
}
