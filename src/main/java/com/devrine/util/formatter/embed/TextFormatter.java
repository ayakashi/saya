package com.devrine.util.formatter.embed;

import com.devrine.constants.enumerations.Language;
import com.devrine.util.db.DBManager;
import sx.blah.discord.handle.obj.IChannel;

import static com.devrine.util.Properties.getLocalizedMessage;
import static com.devrine.util.formatter.embed.BaseFormatter.COLOR_INFO;

public class TextFormatter {
    public static void sendMessage(IChannel channel, String title, String text) {
        final String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            channel.sendMessage(BaseFormatter.createBuilder().appendField(
                    getLocalizedMessage(lang, title),
                    getLocalizedMessage(lang, text),
                    false).withColor(COLOR_INFO).build());
        });
    }

    public static void sendMessageWithCustomText(IChannel channel, String title, String text) {
        final String guildId = channel.getGuild().getStringID();
        DBManager.executeConfig(channel, configDao -> {
            final Language lang = configDao.getLanguage(guildId);
            channel.sendMessage(BaseFormatter.createBuilder().appendField(
                    getLocalizedMessage(lang, title),
                    text,
                    false).withColor(COLOR_INFO).build());
        });
    }
}
