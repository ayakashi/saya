package com.devrine.util.formatter.message;

import com.devrine.constants.enumerations.Language;
import com.devrine.constants.enumerations.command.Command;

import static com.devrine.util.formatter.embed.BaseFormatter.BR;

public class MessageFormatter {
    public static String formatCommand(Language language, Command cmd) {
        return String.format(
              "%-" + Command.getMaxImplementedNameLength() + "s    %-" + Command.getMaxImplementedHelpLength(language) + "s" + BR,
              cmd.getName(),
              cmd.getHelp(language));
    }
}