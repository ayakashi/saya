package com.devrine.util;

import com.devrine.constants.PropertyName;
import com.devrine.constants.enumerations.Language;
import com.devrine.util.db.DBManager;
import com.devrine.util.db.dao.IConfigDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;


public class Properties {
	private static final Logger logger = LoggerFactory.getLogger(Properties.class);
	private static java.util.Properties properties = null;
	private static Map<String, Language> languages = new HashMap<>();

	private static synchronized void getPropertyFile() {
		InputStream input;
		if (properties == null) {
			try {
				properties = new java.util.Properties();
				input = Properties.class.getClassLoader().getResourceAsStream("config/base.properties");

				if (input != null) {
					properties.load(input);
				} else {
					input = new FileInputStream("/etc/config/base.properties");
					properties.load(input);
				}
			} catch (IOException e) {
				logger.error("Can not load properties file.");
			}
		}
	}

	public static synchronized void getAllLanguages(IConfigDao dbWorker) {
		languages.clear();
		languages.putAll(dbWorker.getAllLanguages());
	}

	public static String getProperty(String property) {
		if (properties == null) {
			getPropertyFile();
			getEnvProperties();
		}
		return properties.getProperty(property);
	}

	public static Language getLanguage(String guildId) {
		if (languages.isEmpty()) {
			DBManager.executeConfig(null, Properties::getAllLanguages);
		}
		return languages.get(guildId);
	}

	public static String getLocalizedMessage(String property) {
		return ResourceBundle.getBundle("main", Locale.US, new EncodedControl()).getString(property);
	}

	public static String getLocalizedMessage(Language language, String property) {
		Locale locale;
		if(language != null) {
			locale = Locale.forLanguageTag(language.getName());
		} else {
			locale = Locale.US;
		}
		return ResourceBundle.getBundle("main", locale, new EncodedControl()).getString(property);
	}

	public static synchronized void getEnvProperties() {
		if (properties == null) {
			properties = new java.util.Properties();
		}
		setPropertyIfExists(PropertyName.BASE_URL);
		setPropertyIfExists(PropertyName.BOT_TOKEN);
		setPropertyIfExists(PropertyName.DB_URL);
		setPropertyIfExists(PropertyName.DB_USER);
		setPropertyIfExists(PropertyName.DB_PASS);
	}

	public static void setPropertyIfExists(String name) {
		if (System.getenv(name) != null) {
			properties.setProperty(name, System.getenv(name));
		}
	}
}
