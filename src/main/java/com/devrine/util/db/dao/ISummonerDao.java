package com.devrine.util.db.dao;

import com.devrine.beans.Summoner;
import com.devrine.util.db.IDBWorker;

import java.util.List;

public interface ISummonerDao extends IDBWorker {
    List<Summoner> getAllSummoners();
    boolean updatePuuids();
}
