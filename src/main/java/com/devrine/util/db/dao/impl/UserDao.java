package com.devrine.util.db.dao.impl;

import com.devrine.beans.Summoner;
import com.devrine.beans.User;
import com.devrine.constants.Database;
import com.devrine.constants.enumerations.Language;
import com.devrine.constants.enumerations.Server;
import com.devrine.constants.enumerations.role.GroupRankRole;
import com.devrine.constants.enumerations.role.RankRole;
import com.devrine.util.Properties;
import com.devrine.util.RankUtil;
import com.devrine.util.db.DBManager;
import com.devrine.util.db.DBWorker;
import com.devrine.util.db.dao.IUserDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.Discord4J;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.MessageBuilder;
import sx.blah.discord.util.RateLimitException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDao extends DBWorker implements IUserDao {
    private static final Logger logger = LoggerFactory.getLogger(UserDao.class);

    public boolean isUserInDatabase(User user) {
        PreparedStatement st = null;
        ResultSet rs = null;

        boolean result = false;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement("select count(*) as copies from user where user_id=?");
                st.setString(1, user.getUserId());
                rs = st.executeQuery();
                rs.next();
                if(rs.getInt(1) > 0) {
                    result = true;
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }
        return result;
    }

    public User getUser(String id) {
        PreparedStatement st = null;
        ResultSet rs = null;
        User user = null;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_USER);
                st.setString(1, id);
                rs = st.executeQuery();
                while(rs.next()) {
                    if (user == null) {
                        user = new User(
                                rs.getString("user_id"),
                                rs.getString("discriminator"),
                                rs.getString("name"),
                                rs.getString("country"),
                                rs.getString("city")
                        );
                    }
                    user.getSummoners().add(
                            new Summoner(
                                    rs.getString("puuid"),
                                    rs.getString("summonerName"),
                                    Server.getByName(rs.getString("server")),
                                    rs.getString("summonerName")
                            )
                    );
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return user;
    }

    public boolean addUser(User user) {
        boolean result = false;
        PreparedStatement st = null;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.INSERT_USER);
                st.setString(1, user.getUserId());
                st.setString(2, user.getDiscriminator());
                st.setString(3, user.getName());
                st.setString(4, user.getCountry());
                st.setString(5, user.getCity());
                st.executeUpdate();

                if (user.getSummoners() != null && !user.getSummoners().isEmpty()) {
                    Summoner summoner = user.getSummoners().get(0);
                    st = connection.prepareStatement(Database.INSERT_SUMMONER);
                    st.setString(1, summoner.getPuuid());
                    st.setString(2, summoner.getAccountId());
                    st.setString(3, summoner.getServer().getName());
                    st.setString(4, summoner.getName());
                    st.setString(5, user.getUserId());
                    st.executeUpdate();
                }
                result = true;
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(st);
        }
        return result;
    }

    public List<User> getAllUsers() {
        PreparedStatement st = null;
        ResultSet rs = null;
        User user;
        List<User> users = new ArrayList<>();

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_ALL_USERS);
                rs = st.executeQuery();
                while(rs.next()) {
                    user = new User();
                    user.setUserId(rs.getString("user_id"));
                    user.setDiscriminator(rs.getString("discriminator"));
                    user.setName(rs.getString("name"));

                    users.add(user);
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return users;
    }

    public void updateIds(IGuild guild) {
        PreparedStatement st = null;
        List<IRole> roles;
        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement("UPDATE user SET user_id=? WHERE discriminator=?");
                for (IUser user : guild.getUsers()) {
                    roles = user.getRolesForGuild(guild);
                    if (roles != null && !roles.isEmpty()) {
                        st.setString(1, user.getStringID());
                        st.setString(2, user.getDiscriminator());
                        st.addBatch();
                    }
                }
                st.executeBatch();
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(st);
        }
    }

    public void updateDiscordRanks(IDiscordClient client, IChannel channel, IRole role) {
        IGuild guild = channel.getGuild();
        List<IUser> users = getUsersByRole(guild, role);

        DBManager.executeConfig(channel, configDao -> {
            User dbUser;
            String name;
            Server server;
            int index = 0;
            final Language lang = configDao.getLanguage(guild.getStringID());
            IMessage progressMessage = new MessageBuilder(client)
                    .withChannel(channel)
                    .withContent(Properties.getLocalizedMessage(lang, "command.update.ranks.started"))
                    .build();

            for (IUser user : users) {
                if (index++ % 10 == 0) {
                    progressMessage.edit(
                            Properties.getLocalizedMessage(lang, "command.update.ranks.started") + " "
                                    + index + " "
                                    + Properties.getLocalizedMessage(lang, "main.text.of") + " " +
                                    users.size());
                }
                dbUser = getUser(user.getStringID());
                if (dbUser == null) {
                    RankUtil.removeGroupRankRoles(guild, user);
                    continue;
                }

                for (Summoner summoner : dbUser.getSummoners()) {
                    name = summoner.getName();
                    server = summoner.getServer();
                    if (name != null && server != null) {
                        summoner.setRanks(RankUtil.getRanks(server, name));
                    }
                }

                RankRole rankRole = RankUtil.getMaxSummonersRiftRank(dbUser);
                boolean isSuccessful = false;
                while (!isSuccessful) {
                    try {
                        List<IRole> roleList = guild.getRolesByName(rankRole.getName());
                        if (roleList != null && !roleList.isEmpty()) {
                            IRole discordRankRole = roleList.get(0);
                            RankUtil.changeRankRole(guild, user, discordRankRole);

                            GroupRankRole groupRankRole = GroupRankRole.get(rankRole);
                            if (groupRankRole != null) {
                                List<IRole> groupRoleList = guild.getRolesByName(groupRankRole.getName());
                                if (groupRoleList != null && !groupRoleList.isEmpty()) {
                                    IRole discordGroupRankRole = groupRoleList.get(0);
                                    RankUtil.changeGroupRankRole(guild, user, discordGroupRankRole);
                                }
                            } else {
                                RankUtil.removeGroupRankRoles(guild, user);
                            }
                        }
                        isSuccessful = true;
                    } catch (RateLimitException e) {
                        Discord4J.LOGGER.warn("Discord rate limit. Waiting for {} sec.", e.getRetryDelay() / 1000);
                        try {
                            Thread.sleep(e.getRetryDelay());
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                }
            }

            progressMessage.edit(Properties.getLocalizedMessage(lang, "command.update.ranks.finished"));
        });
    }

    public List<IUser> getUsersByRole(IGuild guild, IRole role) {
        if(role != null) {
            return guild.getUsersByRole(role);
        } else {
            return guild.getUsers();
        }
    }
}
