package com.devrine.util.db.dao;

import com.devrine.constants.enumerations.Language;
import com.devrine.util.db.IDBWorker;
import sx.blah.discord.handle.obj.IGuild;

import java.util.Map;

public interface IConfigDao extends IDBWorker {
    boolean isConfigInDatabase(IGuild guild);
    void addLanguage(IGuild guild, Language lang);
    void addGreetings(IGuild guild, String greetings);
    void updateLanguage(IGuild guild, Language lang);
    void updateGreetings(IGuild guild, String greetings);
    Map<String, Language> getAllLanguages();
    Language getLanguage(String guildId);
    String getGreetings(String guildId);
}
