package com.devrine.util.db.dao;

import com.devrine.beans.User;
import com.devrine.util.db.IDBWorker;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;

import java.util.List;


public interface IUserDao extends IDBWorker {
	boolean isUserInDatabase(User user);
	boolean addUser(User user);
	User getUser(String id);
	List<User> getAllUsers();

	void updateDiscordRanks(IDiscordClient client, IChannel channel, IRole role);

	void updateIds(IGuild guild);
}
