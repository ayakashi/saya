package com.devrine.util.db.dao.impl;

import com.devrine.constants.Database;
import com.devrine.constants.enumerations.Language;
import com.devrine.util.db.DBWorker;
import com.devrine.util.db.dao.IConfigDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.handle.obj.IGuild;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class ConfigDao extends DBWorker implements IConfigDao {
    private static final Logger logger = LoggerFactory.getLogger(ConfigDao.class);

    public boolean isConfigInDatabase(IGuild guild) {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean result = false;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement("select count(*) as copies from config where guild_id=?");
                st.setString(1, guild.getStringID());
                rs = st.executeQuery();
                rs.next();
                if(rs.getInt(1) > 0) {
                    result = true;
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }
        return result;
    }

    public void addLanguage(IGuild guild, Language lang) {
        executeUpdate(Database.INSERT_LANGUAGE,
                guild.getStringID(),
                guild.getName(),
                lang.getName()
        );
    }

    public void addGreetings(IGuild guild, String greetings) {
        executeUpdate(Database.INSERT_GREETINGS,
                guild.getStringID(),
                guild.getName(),
                greetings
        );
    }

    public void updateLanguage(IGuild guild, Language lang) {
        executeUpdate("UPDATE config SET language=? WHERE guild_id=?",
                lang.getName(),
                guild.getStringID()
        );
    }

    public void updateGreetings(IGuild guild, String greetings) {
        executeUpdate("UPDATE config SET welcome_message=? WHERE guild_id=?",
                greetings,
                guild.getStringID()
        );
    }

    public Map<String, Language> getAllLanguages() {
        PreparedStatement st = null;
        ResultSet rs = null;
        Map<String, Language> languages = new HashMap<>();

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_ALL_LANGUAGES);
                rs = st.executeQuery();
                while(rs.next()) {
                    languages.put(rs.getString("guild_id"), Language.getByName(rs.getString("language")));
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return languages;
    }

    public Language getLanguage(String guildId) {
        PreparedStatement st = null;
        ResultSet rs = null;
        Language language = Language.EN;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_LANGUAGE);
                st.setString(1, guildId);
                rs = st.executeQuery();
                if(rs.next()) {
                    language = Language.getByName(rs.getString("language"));
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return language;
    }

    public String getGreetings(String guildId) {
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_GREETINGS);
                st.setString(1, guildId);
                rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getString("welcome_message");
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return null;
    }
}
