package com.devrine.util.db.dao.impl;

import com.devrine.beans.Summoner;
import com.devrine.constants.Database;
import com.devrine.constants.enumerations.Server;
import com.devrine.lolapi.summoner.beans.SummonerDto;
import com.devrine.lolapi.summoner.workers.SummonerWorker;
import com.devrine.util.db.DBWorker;
import com.devrine.util.db.dao.ISummonerDao;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SummonerDao extends DBWorker implements ISummonerDao {
    private static final Logger logger = LoggerFactory.getLogger(SummonerDao.class);

    public List<Summoner> getAllSummoners() {
        PreparedStatement st = null;
        ResultSet rs = null;
        Summoner summoner;
        List<Summoner> summoners = new ArrayList<>();

        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement(Database.SELECT_ALL_SUMMONERS);
                rs = st.executeQuery();
                while(rs.next()) {
                    summoner = new Summoner();
                    summoner.setPuuid(rs.getString("puuid"));
                    summoner.setAccountId(rs.getString("accountId"));
                    summoner.setServer(Server.getByName(rs.getString("server")));
                    summoner.setName(rs.getString("name"));

                    summoners.add(summoner);
                }
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(rs);
            close(st);
        }

        return summoners;
    }

    public boolean updatePuuids() {
        PreparedStatement st = null;
        try {
            if (connection != null && !connection.isClosed()) {
                st = connection.prepareStatement("UPDATE summoner SET puuid=?, accountId=? WHERE server=? and name=?");

                List<Summoner> summoners = getAllSummoners();
                for (Summoner summoner : summoners) {
                    if (StringUtils.isBlank(summoner.getPuuid())) {
                        SummonerDto leagueSummoner = SummonerWorker.summonerByName(summoner.getServer().getEndpoint(), summoner.getName());
                        if (leagueSummoner != null && StringUtils.isNotBlank(leagueSummoner.getPuuid())) {
                            st.setString(1, leagueSummoner.getPuuid());
                            st.setString(2, leagueSummoner.getAccountId());
                            st.setString(3, summoner.getServer().getName());
                            st.setString(4, summoner.getName());
                            st.addBatch();
                        }
                    }
                }
                st.executeBatch();
                return true;
            } else {
                logger.warn(Database.CONNECTION_IS_NOT_READY);
            }
        } catch (SQLException e) {
            logger.error(e.getMessage(), e);
        } finally {
            close(st);
        }
        return false;
    }
}
