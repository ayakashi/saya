package com.devrine.util.db;

import com.devrine.util.db.dao.IConfigDao;
import com.devrine.util.db.dao.ISummonerDao;
import com.devrine.util.db.dao.IUserDao;
import com.devrine.util.db.dao.impl.ConfigDao;
import com.devrine.util.db.dao.impl.SummonerDao;
import com.devrine.util.db.dao.impl.UserDao;
import com.devrine.util.formatter.embed.ErrorFormatter;
import sx.blah.discord.handle.obj.IChannel;

import java.util.function.Consumer;

import static com.devrine.constants.Database.MAX_CONNECTIONS;


public class DBManager {
	private static volatile int connections = 0;

	public static int getConnectionsCount() {
		return connections;
	}

	public static synchronized boolean execute(IChannel channel, Consumer<DBWorker> task) {
		if (connections < MAX_CONNECTIONS) {
			final DBWorker dbWorker = new DBWorker();
			dbWorker.connect();
			connections++;
			try {
				task.accept(dbWorker);
				return true;
			} finally {
				dbWorker.closeConnection();
				connections--;
			}
		} else {
			if (channel != null) {
				ErrorFormatter.sendDBErrorTooManyConnections(channel);
			}
		}
		return false;
	}

	public static synchronized boolean executeUser(IChannel channel, Consumer<IUserDao> task) {
		if (connections < MAX_CONNECTIONS) {
			final IUserDao userDao = new UserDao();
			userDao.connect();
			connections++;
			try {
				task.accept(userDao);
				return true;
			} finally {
				userDao.closeConnection();
				connections--;
			}
		} else {
			if (channel != null) {
				ErrorFormatter.sendDBErrorTooManyConnections(channel);
			}
		}
		return false;
	}

	public static synchronized boolean executeConfig(IChannel channel, Consumer<IConfigDao> task) {
		if (connections < MAX_CONNECTIONS) {
			final IConfigDao configDao = new ConfigDao();
			configDao.connect();
			connections++;
			try {
				task.accept(configDao);
				return true;
			} finally {
				configDao.closeConnection();
				connections--;
			}
		} else {
			if (channel != null) {
				ErrorFormatter.sendDBErrorTooManyConnections(channel);
			}
		}
		return false;
	}

	public static synchronized boolean executeSummoner(IChannel channel, Consumer<ISummonerDao> task) {
		if (connections < MAX_CONNECTIONS) {
			final ISummonerDao summonerDao = new SummonerDao();
			summonerDao.connect();
			connections++;
			try {
				task.accept(summonerDao);
				return true;
			} finally {
				summonerDao.closeConnection();
				connections--;
			}
		} else {
			if (channel != null) {
				ErrorFormatter.sendDBErrorTooManyConnections(channel);
			}
		}
		return false;
	}
}
