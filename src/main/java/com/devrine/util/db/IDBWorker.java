package com.devrine.util.db;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;


public interface IDBWorker {
	void connect();

	void closeConnection();

    void close(Connection connection);
    void close(Statement statement);
    void close(ResultSet rs);

	void executeUpdate(String query, String ... parameters);
}
