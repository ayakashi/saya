package com.devrine.util.db;

import com.devrine.constants.Database;
import com.devrine.constants.PropertyName;
import com.devrine.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;


public class DBWorker implements IDBWorker {
	private static final Logger logger = LoggerFactory.getLogger(DBWorker.class);
	protected Connection connection = null;

	static {
		loadDriver();
	}

	private static void loadDriver() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			logger.error("No MySQL JDBC Driver", e);
		}
	}

	public void connect() {
		try {
			connection = DriverManager.getConnection(
					Properties.getProperty(PropertyName.DB_URL),
					Properties.getProperty(PropertyName.DB_USER),
					Properties.getProperty(PropertyName.DB_PASS)
			);
		} catch (SQLException e) {
			logger.error(Database.CONNECTION_FAILED, e);
		}
	}

	public void closeConnection() {
		close(connection);
	}

	public void close(Connection connection) {
		try {
			if(connection != null && !connection.isClosed()) {
				connection.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void close(Statement statement) {
		try {
			if(statement != null && !statement.isClosed()) {
				statement.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void close(ResultSet rs) {
		try {
			if(rs != null && !rs.isClosed()) {
				rs.close();
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		}
	}

	public void executeUpdate(String query, String ... parameters) {
		PreparedStatement st = null;

		try {
			if (connection != null && !connection.isClosed()) {
				st = connection.prepareStatement(query);
				for (int i=0; i<parameters.length; i++) {
					st.setString(i + 1, parameters[i]);
				}
				st.executeUpdate();
			} else {
				logger.warn(Database.CONNECTION_IS_NOT_READY);
			}
		} catch (SQLException e) {
			logger.error(e.getMessage(), e);
		} finally {
			close(st);
		}
	}
}
