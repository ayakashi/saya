package com.devrine.util;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HtmlHelper {
    private static final Logger logger = LoggerFactory.getLogger(HtmlHelper.class);

    public static String getHTML(String urlToRead) {
        URL url;
        HttpURLConnection conn;
        String line = "";
        try {
            url = new URL(urlToRead);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            line = IOUtils.toString(new InputStreamReader(conn.getInputStream()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return line;
    }

    public static String readText(Element element, String sclass) {
        Elements elements = element.getElementsByClass(sclass);
        if(elements.iterator().hasNext()) {
            return elements.iterator().next().text();
        }
        return StringUtils.EMPTY;
    }
}
