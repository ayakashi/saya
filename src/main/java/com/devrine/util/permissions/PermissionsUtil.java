package com.devrine.util.permissions;

import com.devrine.constants.enumerations.command.Command;
import sx.blah.discord.handle.obj.Permissions;

import java.util.Set;


public class PermissionsUtil {
	public static boolean isManager(Set<Permissions> permissions) {
		return permissions.contains(Permissions.ADMINISTRATOR);
	}

	public static boolean isAllowed(Command cmd, Set<Permissions> permissions) {
		switch (cmd.getMinUserRole()) {
			case GUEST:
				return true;
			case USER:
				return true;
			case USER_1:
				return true;
			case USER_2:
				return true;
			case MANAGER:
				return cmd.getMinUserRole().getId() > 3 && isManager(permissions);
			case ADMIN:
				break;
			default:
				return false;
		}
		return false;
	}
}
