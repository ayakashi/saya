package com.devrine.util;

import com.devrine.beans.Rank;
import com.devrine.beans.Summoner;
import com.devrine.beans.User;
import com.devrine.constants.enumerations.Queue;
import com.devrine.constants.enumerations.Server;
import com.devrine.constants.enumerations.role.GroupRankRole;
import com.devrine.constants.enumerations.role.RankRole;
import com.devrine.lolapi.league.beans.LeaguePositionDto;
import com.devrine.lolapi.league.workers.LeagueWorker;
import com.devrine.lolapi.summoner.beans.SummonerDto;
import com.devrine.lolapi.summoner.workers.SummonerWorker;
import org.apache.commons.lang3.StringUtils;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RankUtil {
    public static List<Rank> getRanks(Server server, String name) {
        List<Rank> ranks = new ArrayList<>();
        SummonerDto summonerDto = SummonerWorker.summonerByName(server.getEndpoint(), name);
        if (summonerDto == null || StringUtils.isEmpty(summonerDto.getId())) {
            return ranks;
        }

        Set<LeaguePositionDto> positions = LeagueWorker.getPositionsBySummonerId(
                server.getEndpoint(),
                summonerDto.getId()
        );
        if (positions.isEmpty()) {
            return ranks;
        }

        for (LeaguePositionDto position : positions) {
            Rank rank = new Rank();
            Queue queue = Queue.getByCode(position.getQueueType());
            rank.setQueue(queue != null ? queue : Queue.UNKNOWN);
            rank.setTier(position.getTier());
            rank.setDivision(position.getRank());
            rank.setLeaguePoints(position.getLeaguePoints());
            rank.setWins(position.getWins());
            rank.setLosses(position.getLosses());
            ranks.add(rank);
        }
        return ranks;
    }

    public static RankRole getMaxSummonersRiftRank(User user) {
        Set<RankRole> rankRoles = new HashSet<>();
        if (user.getSummoners() != null) {
            for (Summoner summoner : user.getSummoners()) {
                if (summoner.getRanks() == null) {
                    continue;
                }
                for (Rank rank : summoner.getRanks()) {
                    if (Queue.RANKED_SOLO_5x5.equals(rank.getQueue()) || Queue.RANKED_FLEX_SR.equals(rank.getQueue())) {
                        rankRoles.add(RankRole.get(rank.getTier() + " " + rank.getDivision()));
                    }
                }
            }
        }

        return rankRoles.stream()
                .min((role1, role2) -> role2.getValue() - role1.getValue())
                .orElse(RankRole.UNRANKED);
    }

    public static void changeRankRole(IGuild guild, IUser user, IRole role) {
        for (IRole iRole : user.getRolesForGuild(guild)) {
            for (RankRole rankRole : RankRole.values()) {
                if (!iRole.equals(role) && iRole.getName().equals(rankRole.getName())) {
                    user.removeRole(iRole);
                    break;
                }
            }
        }

        if (!user.hasRole(role)) {
            user.addRole(role);
        }
    }

    public static void changeGroupRankRole(IGuild guild, IUser user, IRole role) {
        for (IRole iRole : user.getRolesForGuild(guild)) {
            for (GroupRankRole rankRole : GroupRankRole.values()) {
                if (!iRole.equals(role) && iRole.getName().equals(rankRole.getName())) {
                    user.removeRole(iRole);
                    break;
                }
            }
        }

        if (!user.hasRole(role)) {
            user.addRole(role);
        }
    }

    public static void removeGroupRankRoles(IGuild guild, IUser user) {
        for (IRole iRole : user.getRolesForGuild(guild)) {
            for (GroupRankRole rankRole : GroupRankRole.values()) {
                if (iRole.getName().equals(rankRole.getName())) {
                    user.removeRole(iRole);
                    break;
                }
            }
        }
    }
}
