package com.devrine.util;

import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;

import java.util.List;

public class Util {
    public static String getChannelMention(IGuild guild, String name) {
        List<IChannel> channels = guild.getChannelsByName(name);
        if (channels != null && !channels.isEmpty()) {
            return channels.get(0).mention();
        }
        return "#" + name;
    }

    public static boolean isMessageForBot(IMessage message) {
        for(IUser user : message.getMentions()) {
            if(user.isBot()) {
                return true;
            }
        }
        return false;
    }

    public static boolean isBotChannel(IChannel channel) {
        if ("botcheck".equals(channel.getName())) {
            return true;
        }
        return false;
    }
}
