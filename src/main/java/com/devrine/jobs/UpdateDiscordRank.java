package com.devrine.jobs;

import com.devrine.util.Worker;
import com.devrine.util.db.DBManager;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;


public class UpdateDiscordRank implements Job {
	private static final Logger logger = LoggerFactory.getLogger(UpdateDiscordRank.class);

	@Override
	public void execute(JobExecutionContext jobExecutionContext) {
		IDiscordClient client = Worker.getClient();
		JobDataMap data = jobExecutionContext.getJobDetail().getJobDataMap();
		IChannel channel = client.getChannelByID(data.getLong("channel"));

		String name = client.getGuildByID(data.getLong("guild")).getName();
		logger.info("Rank update started for {}", name);
		DBManager.executeUser(null, userDao -> userDao.updateDiscordRanks(client, channel,null));
		logger.info("Rank update finished for {}", name);
	}
}
